import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { fetchallusers, IUser } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url = 'https://reqres.in'

  constructor(private http: HttpClient) { }

  getAllUsers(){
    return this.http.get<fetchallusers>(`${this.url}/api/users?page=2`)
            .pipe(
              map( this.transformUser )
            )
  }

  private transformUser( res: fetchallusers): IUser[] {

    const Userlist: IUser[] = res.data.map( user => {
      console.log(res.data.length);
      
      return {
        num: '1',
      id: user.id,
      email: user.email,
      first_name: user.first_name,
      last_name: user.last_name,
      avatar: user.avatar
      }
      
    })
    return Userlist;

  }

}
