import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SharedModule } from './components/shared/shared.module';
import { HomeComponent } from './components/dashboard/home/home.component';
import { FeatursComponent } from './components/dashboard/featurs/featurs.component';
import { PricingComponent } from './components/dashboard/pricing/pricing.component';
import { AboutComponent } from './components/dashboard/about/about.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FeatursComponent,
    PricingComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
