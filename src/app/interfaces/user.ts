export interface fetchallusers {
  page: number,
  per_page: number,
  total: number,
  total_pages: number,
  data: smallUser[]
}

export interface smallUser {
  id: number,
  email: string,
  first_name: string,
  last_name: string,
  avatar: string,
}

export interface IUser {
  num: string,
  id: number,
  email: string,
  first_name: string,
  last_name: string,
  avatar: string,
}
