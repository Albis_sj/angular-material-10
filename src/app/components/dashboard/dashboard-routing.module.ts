import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { DashboardComponent } from './dashboard.component';
import { FeatursComponent } from './featurs/featurs.component';
import { HomeComponent } from './home/home.component';
import { PricingComponent } from './pricing/pricing.component';

const routes: Routes = [
  { path: '', component: DashboardComponent,
    children:
  [
    { path: '', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'features', component: FeatursComponent },
    { path: 'pricing', component: PricingComponent},
  ] }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
